<#ftl encoding='UTF-8'>
<!doctype html>
<html lang="en">
<head>
    <title>Пользователи</title>
    <meta charset="UTF-8">
</head>
<body>
<form method="post" action="/signUp">
    <label for="id">Введите ваш id</label>
    <input id="id" name="id" placeholder="ID">
    <br>
    <br>
    <label for="firstName">Введите ваше имя:</label>
    <input id="firstName" name="firstName" placeholder="Имя">
    <br>
    <br>
    <label for="lastName">Введите вашу фамилию:</label>
    <input id="lastName" name="lastName" placeholder="Фамилия">
    <br>
    <br>
    <label for="email">Введите вашу почту:</label>
    <input id="email" type="email" name="email" placeholder="Почта">
    <br>
    <br>
    <label for="password">Введите ваш пароль:</label>
    <input id="password" type="password" name="password" placeholder="Пароль">
    <br>
    <br>
    <label for="age">Введите ваш возраст:</label>
    <input id="age" name="age" placeholder="Возраст">
    <br>
    <br>
    <input type="submit" value="Sign Up">
</form>
</body>
</html>