package org.example.services;

import org.example.dto.AccountDto;
import org.example.repositories.AccountsJdbcRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.example.dto.AccountDto.from;

@Service
public class AccountsService {

    private final AccountsJdbcRepositoryImpl accountsJdbcRepository;

    @Autowired
    public AccountsService(AccountsJdbcRepositoryImpl accountsJdbcRepository) {
        this.accountsJdbcRepository = accountsJdbcRepository;
    }

    public List<AccountDto> getAllAccounts() {
        //accountsJdbcRepository.init();
        return from(accountsJdbcRepository.findAll());
    }
}
