package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.SignUpForm;
import org.example.models.Account;
import org.example.repositories.AccountsJdbcRepositoryImpl;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {

    private final AccountsJdbcRepositoryImpl accountsJdbcRepository;

    public SignUpService(AccountsJdbcRepositoryImpl accountsJdbcRepository) {
        this.accountsJdbcRepository = accountsJdbcRepository;
    }

    public void signUp(SignUpForm signUpForm){
        Account account = Account.builder()
                .id(signUpForm.getId())
                .firstName(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail())
                .password(signUpForm.getPassword())
                .age(signUpForm.getAge())
                .build();
        accountsJdbcRepository.save(account);
    }
}
