package org.example.repositories;

import org.example.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AccountsJdbcRepositoryImpl implements BaseRepository{

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public AccountsJdbcRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    @Autowired
    private JdbcTemplate jdbcTemplate;


    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from accounts order by id";
    //language=SQL
    private static final String SQL_INSERT = "insert into accounts (id, first_name, last_name, email, password, age)" +
            " values (:id, :firstName, :lastName, :email, :password, :age)";
    //language=SQL
    private static final String SQL_CREATE_TABLE = "create table accounts (id INT, first_name VARCHAR(20)," +
            " last_name VARCHAR(30), email VARCHAR(30), password VARCHAR(30), age INT)";
    //language=SQL
    private static final String SQL_INSERT_WITH_ID = "INSERT INTO accounts (id, first_name, last_Name, email, password, age)" +
            " VALUES(?,?,?,?,?,?)";
    //language=SQL
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS accounts";
    private final RowMapper<Account> accountRowMapper = ((rs, rowNum) -> Account.builder()
            .id(rs.getInt("id"))
            .firstName(rs.getString("first_name"))
            .lastName(rs.getString("last_name"))
            .email(rs.getString("email"))
            .password(rs.getString("password"))
            .age(rs.getInt("age"))
            .build());

    public void init() {
        jdbcTemplate.execute(DROP_TABLE);
        jdbcTemplate.execute(SQL_CREATE_TABLE);
        jdbcTemplate.update(SQL_INSERT_WITH_ID, 1, "Marat", "Bermeleyev", "mbermeleyev@bk.ru", "qwert001", 22);
        jdbcTemplate.update(SQL_INSERT_WITH_ID, 2, "Evgeniy", "Kuchin", "evgeniy.k@gmail.ru", "qwert002", 30);
        jdbcTemplate.update(SQL_INSERT_WITH_ID, 3, "Maxim", "Ilyin", "maxIl@mail.ru", "qwert003", 25);
    }

    @Override
    public void save(Account account) {
        //KeyHolder keyHolder = new GeneratedKeyHolder();

        Map<String, Object> values = new HashMap<>();
        values.put("id", account.getId());
        values.put("firstName", account.getFirstName());
        values.put("lastName", account.getLastName());
        values.put("email", account.getEmail());
        values.put("password", account.getPassword());
        values.put("age", account.getAge());
        //account.setId(keyHolder.getKeyAs(Integer.class));

        SqlParameterSource parameterSource = new MapSqlParameterSource(values);

        namedParameterJdbcTemplate.update(SQL_INSERT, parameterSource);
        //, keyHolder, new String[]{"id"}
    }

    @Override
    public List<Account> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }
}
