package org.example.repositories;

import org.example.models.Account;

import java.util.List;

public interface BaseRepository {
    void save(Account account);
    List<Account> findAll();
}
