package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class App {

    public static void main( String[] args ) {
        City city = new City("city", 1500, true, true);

        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        /*Transport plane = new Plane("Plane", 600.0, 500, 136.3);
        Transport ship = new Ship("Ship", 500.0, 60, 115.3);
        Transport truck = new Truck("Truck", 700.0, 40, 500.0);
        Transport ship2 = new Ship("Ship", 500.0, 60, 110.3);
        Transport plane2 = new Plane("Plane", 600.0, 500, 116.3);
        new Logistics(plane, ship, truck, ship2, plane2);
*/
        //TransportFactory factory = context.getBean(TransportFactory.class);

        //System.out.println(logistics);
        //System.out.println(factory.getTransport(city, 3700, 5));
        Logistics logistics = context.getBean(Logistics.class);
        System.out.println(logistics.getShipping(city, 3700, 5));
    }
}
