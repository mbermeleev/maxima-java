package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class Logistics {
    @Autowired
    private TransportFactory factory;
    public TransportFactory getFactory() {
        return factory;
    }
    private Transport[] vehicles;
    private Logistics(){};
    public Logistics(Transport... vehicles) {
        this.vehicles = vehicles;
    }
    public void setVehicles(Transport[] vehicles) {
        this.vehicles = vehicles;
    }
    public Transport[] getVehicles() {
        return this.vehicles;
    }
    public Transport getShipping(City city, int weight, int hours) {
        double minPrice = 0.0;
        Transport vehicle = null;
        if (vehicles == null){
            return factory.getTransport(city, 3700, 6);
        }else {
            for (int i = 0; i < this.vehicles.length; ++i) {
                if (this.isShippingAvailable(city, this.vehicles[i], weight, hours) && (minPrice == 0.0 || minPrice > (double) this.vehicles[i].getPrice(city))) {
                    minPrice = this.vehicles[i].getPrice(city);
                    vehicle = this.vehicles[i];
                }
            }
            return vehicle;
        }
    }
    private boolean isShippingAvailable(City city, Transport transport, int weight, int hours) {
        int deliveryTime = city.getDistanceKm() / transport.getSpeed();
        return deliveryTime <= hours && transport.getCapacity() >= (double)weight && !transport.isRepairing() && transport.getPrice(city) != 0.0F;
    }
}
