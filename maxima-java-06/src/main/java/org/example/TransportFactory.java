package org.example;

import org.springframework.stereotype.Component;
@Component
public class TransportFactory {
    Transport getTransport(City city, int weight, int hours) {
        double capacity = capacityRounding(weight);
        double speed = speedRounding(city, hours);
        if (city.getDistanceKm() / hours <= 40 && city.isOnWater()) {
            return new Ship("Ship", capacity, (int)speed, 115.0);
        } else {
            return (Transport)(city.getDistanceKm() / hours > 120 && city.hasAirport() ? new Plane("Plane", capacity, (int)speed, 200.0) : new Truck("Truck", capacity, (int)speed, 100.0));
        }
    }
    private static double speedRounding(City city, int hours) {
        double speed = (city.getDistanceKm() / hours);
        return speed % 10.0 != 0.0 ? Math.ceil(speed / 10.0) * 10.0 : speed;
    }
    private static double capacityRounding(int weight) {
        return (double)weight % 500.0 != 0.0 ? Math.ceil((double)weight / 500.0) * 500.0 : (double)weight;
    }
}
