package org.example;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class App
{
    public static void main( String[] args ) {
        /* замена друг на друга значений двух переменных

        int a = 3;
        int b = 5;
        int c;

        System.out.println(a);
        System.out.println(b);
        System.out.println();

        c = a;
        a = b;
        b = c;

        System.out.println(a);
        System.out.println(b);
        System.out.println();
        */
        //System.out.println(fibonacci(6));
        //System.out.println(daysCount(2,4));

        //System.out.println(isPrime(4));

        //System.out.println(maxDigitsSumPosition(new int[]{1,23,456,7070,10000,1001,1212121212}));
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String formattedString = date.format(formatter);
        System.out.println(formattedString);
    }

    /* Фунция определяющая n-ный элемент последовательности Фибоначчи
    public static long fibonaссi(int N) {

        return N <= 1 ? N : fibonaссi(N - 1) + fibonaссi(N - 2);

    }
     */

    /* Функция определяющая n-ый элемент последовательности Фибоначчи
       с помощью конструкции if - else if - else

    public static long fibonazzi(int N) {

        if (N == 0) {

            return 0;

        } else if (N <= 2) {

            return 1;

        } else {

            return fibonazzi(N - 1) + fibonazzi(N - 2);

        }
    }
    */

    /* Вывод количества дней в месяце зная его порядковый номер и год

    public static int daysCount(int month, int year){
        int results = 0;
        if (month >= 13){
            System.err.println("This month doesn't have in this year");
            System.exit(255);
        }
        switch(month){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                results = 31;
                break;
            case 2:
                return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0 ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                results = 30;
                break;
        }
        return results;
    }*/

    /* Функция на определение простое число или нет
    public static int isPrime(int n) {
        if (n < 1){
            System.err.println("Incorrect number!");
            System.exit(255);
        }

        int x = 2;
        while((x * x) <= n){
            if(n % x == 0){
                return x;
            }
            x++;
        }
        return n = 0;
    }
     */

    public static int maxDigitsSumPosition(int[] arr){

        int digitSum = 0;
        int indexOfMaxSumPosition = 0;
        int temp = 0;
        for (int i = 0; i < arr.length;) {

            while (arr[i] != 0){
                digitSum = digitSum + arr[i] % 10;
                arr[i] = arr[i]/10;
                break;
            } if (arr[i] == 0){

                if (temp <= digitSum) {
                temp = digitSum;
                indexOfMaxSumPosition = i;
                }

                digitSum = 0;
                i++;
            }

        }

        return indexOfMaxSumPosition;
    }

}
