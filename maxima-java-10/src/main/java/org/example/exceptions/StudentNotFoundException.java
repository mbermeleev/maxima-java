package org.example.exceptions;

public class StudentNotFoundException extends RuntimeException{

    public StudentNotFoundException() {
        super("Student with this id haven't in data base!");
    }
}
