package org.example.exceptions;

public class MentorNotFoundException extends RuntimeException{

    public MentorNotFoundException() {
        super("Mentor with this id not found!");
    }
}
