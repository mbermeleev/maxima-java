package org.example.repositories;

import org.example.models.Mentor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MentorsRepository extends JpaRepository<Mentor, Long> {
}
