package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.models.Mentor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MentorDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Integer age;

    public static MentorDto from(Mentor mentor) {
        return MentorDto.builder()
                .id(mentor.getId())
                .firstName(mentor.getFirstName())
                .lastName(mentor.getLastName())
                .email(mentor.getEmail())
                .build();
    }

    public static List<MentorDto> from(List<Mentor> mentors){
        return mentors.stream().map(MentorDto::from).collect(Collectors.toList());
    }
}
