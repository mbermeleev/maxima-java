package org.example.services;

import org.example.dto.StudentDto;
import org.example.models.Student;

import java.util.List;

public interface StudentsService {

    List<StudentDto> getAllStudents();

    StudentDto addStudent(StudentDto studentDto);

    StudentDto updateStudent(Long studentId, StudentDto studentDto);

    void deleteStudent(Long studentId);

    StudentDto readStudent(Long studentId);
}
