package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.repositories.CoursesRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
}
