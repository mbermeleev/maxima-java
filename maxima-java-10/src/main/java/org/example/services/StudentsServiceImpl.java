package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.StudentDto;
import org.example.exceptions.StudentNotFoundException;
import org.example.models.Student;
import org.example.repositories.StudentsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.example.dto.StudentDto.from;

@RequiredArgsConstructor
@Service
public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    @Override
    public List<StudentDto> getAllStudents() {
        return from(studentsRepository.findAll());
    }

    @Override
    public StudentDto addStudent(StudentDto studentDto) {

        Student newStudent = Student.builder()
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .email(studentDto.getEmail())
                .age(studentDto.getAge())
                .build();

        studentsRepository.save(newStudent);
        return from(newStudent);
    }

    @Override
    public StudentDto updateStudent(Long studentId, StudentDto studentDto) {
        studentsRepository.findById(studentId).orElseThrow(StudentNotFoundException::new);

        Student existedStudent = studentsRepository.getById(studentId);
        existedStudent.setFirstName(studentDto.getFirstName());
        existedStudent.setLastName(studentDto.getLastName());
        existedStudent.setAge(studentDto.getAge());
        existedStudent.setEmail(studentDto.getEmail());

        studentsRepository.save(existedStudent);
        return from(existedStudent);
    }

    @Override
    public void deleteStudent(Long studentId) {
        studentsRepository.deleteById(studentId);
    }

    @Override
    public StudentDto readStudent(Long studentId) {
        return StudentDto.from(studentsRepository.findById(studentId).orElseThrow(StudentNotFoundException::new));
    }
}
