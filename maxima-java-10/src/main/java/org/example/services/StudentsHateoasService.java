package org.example.services;

import org.example.models.Student;

import java.util.List;

public interface StudentsHateoasService {

    List<Student> getAllStudents();

    Student addStudent(Student student);

    Student updateStudent(Long studentId, Student student);

    void deleteStudent(Long studentId);

    Student readStudent(Long studentId);
}
