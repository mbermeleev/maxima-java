package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.repositories.MentorsRepository;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class MentorsServiceImpl implements MentorsService {

    private final MentorsRepository mentorsRepository;
}
