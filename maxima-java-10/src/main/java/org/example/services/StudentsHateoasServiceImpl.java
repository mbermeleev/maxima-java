package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.StudentDto;
import org.example.exceptions.StudentNotFoundException;
import org.example.models.Student;
import org.example.repositories.StudentsRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.example.dto.StudentDto.from;

@Service
@RequiredArgsConstructor
public class StudentsHateoasServiceImpl implements StudentsHateoasService {

    private final StudentsRepository repository;

    @Override
    public List<Student> getAllStudents() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    public Student addStudent(Student student) {
        Student newStudent = Student.builder()
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .email(student.getEmail())
                .age(student.getAge())
                .build();

        repository.save(newStudent);
        return newStudent;
    }

    @Override
    public Student updateStudent(Long studentId, Student student) {
        repository.findById(studentId).orElseThrow(StudentNotFoundException::new);

        Student existedStudent = repository.getById(studentId);
        existedStudent.setFirstName(student.getFirstName());
        existedStudent.setLastName(student.getLastName());
        existedStudent.setAge(student.getAge());
        existedStudent.setEmail(student.getEmail());

        repository.save(existedStudent);
        return existedStudent;
    }

    @Override
    public void deleteStudent(Long studentId) {
        repository.deleteById(studentId);
    }

    @Override
    public Student readStudent(Long studentId) {
        return repository.findById(studentId).orElseThrow(StudentNotFoundException::new);
    }
}
