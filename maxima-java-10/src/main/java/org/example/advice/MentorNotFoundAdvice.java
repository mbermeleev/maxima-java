package org.example.advice;

import org.example.exceptions.MentorNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class MentorNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(MentorNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String StudentNotFoundHandler(MentorNotFoundException exception) {
        return exception.getMessage();
    }
}
