package org.example.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"students", "mentor"})
@EqualsAndHashCode(exclude = {"students", "mentor"})
@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private Integer cost;

   /* @ManyToMany(mappedBy = "courses")
    private List<Student> students;*/

    @ManyToOne
    @JoinColumn(name = "mentor_id")
    private Mentor mentor;
}
