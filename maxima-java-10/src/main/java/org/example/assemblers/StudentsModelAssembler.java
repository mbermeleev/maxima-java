package org.example.assemblers;

import org.example.controllers.StudentsHateoasController;
import org.example.models.Student;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class StudentsModelAssembler implements RepresentationModelAssembler<Student, EntityModel<Student>> {

    @Override
    public EntityModel<Student> toModel(Student entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(StudentsHateoasController.class).readStudent(entity.getId())).withSelfRel(),
                linkTo(methodOn(StudentsHateoasController.class).getAllStudents()).withRel("group"));
    }

    @Override
    public CollectionModel<EntityModel<Student>> toCollectionModel(Iterable<? extends Student> entities) {
        List<EntityModel<Student>> list = new ArrayList<>();
        for (Student entity: entities) {
                list.add(toModel(entity));
        }
        return CollectionModel.of(list,
                linkTo(methodOn(StudentsHateoasController.class).getAllStudents()).withSelfRel());
    }
}
