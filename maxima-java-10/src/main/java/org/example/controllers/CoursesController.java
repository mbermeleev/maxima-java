package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.services.CoursesService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/courses")
public class CoursesController {

    private final CoursesService coursesService;
}
