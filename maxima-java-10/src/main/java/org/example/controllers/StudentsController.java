package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.dto.StudentDto;
import org.example.dto.StudentsResponse;
import org.example.services.StudentsService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentsController {

    private final StudentsService studentsService;

    @RequestMapping(method = RequestMethod.GET)
    public StudentsResponse getAllStudents() {
        return StudentsResponse.builder()
                .dtoList(studentsService.getAllStudents())
                .build();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto addStudent(@RequestBody StudentDto student) {
        return studentsService.addStudent(student);
    }

    @RequestMapping(method = RequestMethod.PATCH, value = "/{student_id}")
    public StudentDto updateStudent(@RequestBody StudentDto student, @PathVariable ("student_id") Long studentId) {
            return studentsService.updateStudent(studentId, student);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{student_id}")
    public void deleteStudent(@PathVariable ("student_id") Long studentId) {
        studentsService.deleteStudent(studentId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{student_id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentDto readStudent(@PathVariable("student_id") Long student_id) {
        return studentsService.readStudent(student_id);
    }

}
