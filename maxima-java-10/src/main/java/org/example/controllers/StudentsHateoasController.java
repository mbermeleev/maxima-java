package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.assemblers.StudentsModelAssembler;
import org.example.models.Student;
import org.example.services.StudentsHateoasService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hateoas/students")
public class StudentsHateoasController {

    private final StudentsHateoasService studentsService;
    private final StudentsModelAssembler studentsModelAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public CollectionModel<EntityModel<Student>> getAllStudents() {
        return studentsModelAssembler.toCollectionModel(studentsService.getAllStudents());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{student_id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityModel<Student> readStudent(@PathVariable("student_id") Long student_id) {
        return studentsModelAssembler.toModel(studentsService.readStudent(student_id));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public EntityModel<Student> addStudent(@RequestBody Student student) {
        return studentsModelAssembler.toModel(studentsService.addStudent(student));
    }

    @RequestMapping(method = RequestMethod.PATCH, value = "/{student_id}")
    public EntityModel<Student> updateStudent(@RequestBody Student student, @PathVariable ("student_id") Long studentId) {
        return studentsModelAssembler.toModel(studentsService.updateStudent(studentId, student));
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{student_id}")
    public void deleteStudent(@PathVariable ("student_id") Long studentId) {
        studentsService.deleteStudent(studentId);
    }
}
