package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.services.MentorsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mentors")
public class MentorsController {

    private final MentorsService mentorsService;
}
