Описание

а) по rest:
    1) все методы можно протестировать в `requests.http`
    2) `requests.http` находится в папке `resourses` в пакете `http`

б) по hateoas:
    1) все методы можно протестировать в `request.http`
    2) `hateoasRequest.http` находится в папке `resourses` в пакете `http`
    3) единственное, что не так работает, это метод `updateStudent` в hateoas,
        выдает ошибку `hibernateLazyInitializer`, 
        при том информацию о студенте он кладет в бд правильно