package org.example;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
public class StreamTransformer implements Transformable{
    @Override
    public void transform(String fileIn, String fileOut) throws IOException {

        FileInputStream stream = new FileInputStream(fileIn);
        StringBuilder result = new StringBuilder();
        try{
            int r;
            do{
                r = stream.read();
                result.append((char) r);
            }while(r != -1);
            stream.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        System.out.println(result); // toString() - преобразует число в строку

        try {
            FileOutputStream outStream = new FileOutputStream(fileOut);
            String modified = String.valueOf(result).replace(" ", " weight ");
            String modified2 = modified.replace("e weight", "e cat");
            String modified3 = modified2.replace(";", "kg;");
            outStream.write(modified3.getBytes(StandardCharsets.UTF_8), 0, modified3.length());
            outStream.flush();
            outStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
