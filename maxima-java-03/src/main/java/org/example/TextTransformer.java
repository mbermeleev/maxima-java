package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class TextTransformer implements Transformable{
    @Override
    public void transform(String fileIn, String fileOut) throws IOException {
        /*FileReader reader = new FileReader(fileIn);
        int i;
        int counter = 0;
        while((i = reader.read()) != -1 && counter < 150){
            counter++;
            System.out.print((char) i);
        }
        reader.close(); // можно не закрывать поток чтения файла, но лучше закрывать
        System.out.println();*/

        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileIn));
        String line;
        StringBuilder builder = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null){
            builder.append(line);
            builder.append("\n");
        }
        bufferedReader.close();

        String text = String.valueOf(builder);

        String[] textArray = text.split("\n");

        for (int i = 0; i < textArray.length; i++){
            String sentence = textArray[i];
            String[] wordsArray = sentence.split(";");
            String[] newWordsArray = {wordsArray[2], wordsArray[0], wordsArray[1]};
            if (newWordsArray[0].equals("true")) {
                newWordsArray[0] = "Сердитый";
            }else {
                newWordsArray[0] = "Дружелюбный";
            }
            textArray[i] = Arrays.toString(newWordsArray);
        }

        FileWriter writer = new FileWriter(fileOut, true);//append: false - перезапись файла польносью
        for (String s : textArray) {
            String modified = s.replaceAll(", ", " ");
            String modified2 = modified.replaceAll(" ", " весом ");
            String modified3 = modified2.replaceAll("й весом", "й кот");
            String modified4 = modified3.replaceAll("]", "кг;\n");
            String modified5 = modified4.substring(1);
            writer.write(modified5);
        }
        writer.flush(); //принудительный сброс буфера
        writer.close(); //нужно закрывать поток записи файла, обязательно!!!
    }
}
