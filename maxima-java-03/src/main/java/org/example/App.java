package org.example;

import java.io.IOException;
import java.util.ArrayList;

public class App {

    public static void main( String[] args ) throws Exception {
        //System.out.println(Birthday.getAge(2000, 2, 23));

        //System.out.println(Birthday.nextBirthday(2000, 2, 23));

        //System.out.println(CatFactory.createCat("Murzik", 10));

        TextTransformer textTransformer = new TextTransformer();

        textTransformer.transform("Cat.csv", "Cat1.csv");

        //StreamTransformer transformer = new StreamTransformer();

        //transformer.transform("Cat.csv", "Cat2.csv");

        /*QueueKitchen<Cat> queueKitchen = new QueueKitchen<>();

        Cat murzik = new Cat("Murzik", 10, false);
        Cat iskra = new Cat("Iskra", 7, false);
        Cat barsik = new Cat("Barsik", 3, false);

        queueKitchen.add(murzik);
        queueKitchen.add(iskra);
        queueKitchen.add(barsik);

        queueKitchen.feed();
        queueKitchen.feed();
        queueKitchen.feed();

        StackKitchen<Dog> stackKitchen = new StackKitchen<>();

        Dog sharik = new Dog("Sharik", 15, false);
        Dog bobik = new Dog("Bobik", 40, false);
        Dog richard = new Dog("Richard", 8, false);

        stackKitchen.add(sharik);
        stackKitchen.add(bobik);
        stackKitchen.add(richard);

        stackKitchen.feed();
        stackKitchen.feed();
        stackKitchen.feed();*/

        /*Cat murzik = new Cat("Murzik", 10, false);
        Cat iskra = new Cat("Iskra", 7, false);
        Cat barsik = new Cat("Barsik", 3, false);

        CatStatistics statistics = new CatStatistics();

        ArrayList <Cat> cats = new ArrayList<>();

        cats.add(murzik);
        cats.add(iskra);
        cats.add(barsik);

        System.out.println(statistics.sortByNameAscending(cats));
        System.out.println(statistics.sortByWeightDescending(cats));
        System.out.println(statistics.removeFirstAndLast(cats));
        System.out.println(statistics.findFirstNonAngryCat(cats));
        System.out.println(statistics.getCommonWeight(cats, false));
        System.out.println(statistics.groupCatsByFirstLetter(cats));*/
    }
}
