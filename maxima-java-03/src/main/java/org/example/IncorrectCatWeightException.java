package org.example;

public class IncorrectCatWeightException extends Exception{
    public IncorrectCatWeightException(int weight) {
        super(String.valueOf(weight));
    }
}
