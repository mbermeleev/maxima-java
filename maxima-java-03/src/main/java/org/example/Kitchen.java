package org.example;

import java.util.ArrayList;

public abstract class Kitchen<T> implements AnimalKitchen<T>{
    private ArrayList<T> animals = new ArrayList<>();
    public ArrayList<T> getAnimals() {
        return animals;
    }
    public void add(T animal){
        getAnimals().add(animal);
    }
    @Override
    abstract public void feed();
}
