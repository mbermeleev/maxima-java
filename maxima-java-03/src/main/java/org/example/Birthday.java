package org.example;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;


public class Birthday {
    public static int getAge(int year, int month, int date){
        LocalDate today = LocalDate.now();

        LocalDate dateOfBirth = LocalDate.of(year, month, date);

        return (int) ChronoUnit.DAYS.between(dateOfBirth, today);
    }

    public static LocalDate nextBirthday(int year, int month, int date){
        LocalDate today = LocalDate.now();
        int currentAge = getAge(year, month, date);
        int numberDaysUntilTheNextBirthday = (int) (Math.ceil((double) currentAge/1000) * 1000) - currentAge;

        LocalDate dateOfNextBirthday = today.plus(numberDaysUntilTheNextBirthday, ChronoUnit.DAYS);

        return dateOfNextBirthday;
    }
}
