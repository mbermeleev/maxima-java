package org.example;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DynamicPage {

    public void createPage(String filename) throws IOException, IncorrectCatWeightException, TemplateException {
        FileWriter writer = new FileWriter("result.html", false);

        String resourcesPath = "templates";

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);
        configuration.setDirectoryForTemplateLoading(new File(resourcesPath));
        configuration.setDefaultEncoding("UTF-8");

        Map root = new HashMap<>();
        root.put("title", "Данные кота");

        Cat cat = new Cat("Iskra", 5, false);
        root.put("cat", cat);

        Template template = configuration.getTemplate("index.html");
        template.process(root, writer);

        writer.flush();
        writer.close();
    }
}
