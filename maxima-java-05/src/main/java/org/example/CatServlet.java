package org.example;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/cat")
public class CatServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String name = request.getParameter("name");
        int weight = Integer.parseInt(request.getParameter("weight"));
        boolean isAngry = Boolean.parseBoolean(request.getParameter("isAngry"));

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = response.getWriter();

        writer.write(String.format("<h1>Cats name %s </h1>", name));
        writer.write(String.format("<h2>Cats weight %s </h2>", weight));
        writer.write(String.format("<h3>Cat is Angry? %s </h3>", isAngry));

        writer.flush();
        writer.close();
    }

}