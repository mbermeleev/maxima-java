package org.example.security.utils;

import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

@UtilityClass
public class SecurityUtils {
    public final String ID_CLAIM = "id";
    public final String EMAIL_CLAIM = "email";
    public final String ROLE_CLAIM = "role";
    public final String STATE_CLAIM = "state";
    public final String FIRST_NAME_CLAIM = "firstName";
    public final String LAST_NAME_CLAIM = "lastName";

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public Jwt getJwt() {
        return (Jwt) getAuthentication().getPrincipal();
    }

    public String getId() {
        return getJwt().getClaimAsString(ID_CLAIM);
    }
}
