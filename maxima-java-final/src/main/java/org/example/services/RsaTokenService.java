package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.AccountDto;
import org.example.exceptions.AccountHaveRoleUserException;
import org.example.exceptions.AccountNotConfirmedException;
import org.example.models.Role;
import org.example.models.State;
import org.example.security.utils.SecurityUtils;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RsaTokenService {

    private final JwtEncoder jwtEncoder;

    public String generateToken(AccountDto accountDto) {
        if (accountDto.getRole() == Role.USER) {
            throw new AccountHaveRoleUserException();
        } else if (accountDto.getState() == State.NOT_CONFIRMED) {
            throw new AccountNotConfirmedException();
        } else {
            JwtClaimsSet claims = JwtClaimsSet.builder()
                    .claim(SecurityUtils.ID_CLAIM, accountDto.getId())
                    .claim(SecurityUtils.FIRST_NAME_CLAIM, accountDto.getFirstName())
                    .claim(SecurityUtils.LAST_NAME_CLAIM, accountDto.getLastName())
                    .claim(SecurityUtils.EMAIL_CLAIM, accountDto.getEmail())
                    .claim(SecurityUtils.ROLE_CLAIM, String.valueOf(accountDto.getRole()))
                    .claim(SecurityUtils.STATE_CLAIM, String.valueOf(accountDto.getState()))
                    .build();
            return jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
        }
    }
}
