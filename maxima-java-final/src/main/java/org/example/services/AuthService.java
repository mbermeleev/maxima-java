package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.AuthDTO;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final AccountsService accountsService;
    private final PasswordEncoder passwordEncoder;
    private final RsaTokenService rsaTokenService;

    public String authorize(AuthDTO authDTO) {
        var account = accountsService.getByEmail(authDTO.getUsername());
        if(!passwordEncoder.matches(authDTO.getPassword(),account.getPassword())) {
            throw new IllegalStateException("WRONG PASSWORD!!!");
        }
        return rsaTokenService.generateToken(account);
    }
}
