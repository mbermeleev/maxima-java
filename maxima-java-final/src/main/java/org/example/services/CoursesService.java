package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.AccountDto;
import org.example.dto.CourseDto;
import org.example.dto.LessonDto;
import org.example.exceptions.CourseNotFoundException;
import org.example.mappers.AccountMapper;
import org.example.mappers.CourseMapper;
import org.example.mappers.LessonMapper;
import org.example.models.Account;
import org.example.models.Course;
import org.example.models.Lesson;
import org.example.models.Role;
import org.example.repositories.CoursesRepository;
import org.example.repositories.LessonsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CoursesService {
    private final CoursesRepository coursesRepository;

    private final AccountMapper accountMapper;
    private final LessonsRepository lessonsRepository;
    private final LessonMapper lessonMapper;
    private final CourseMapper courseMapper;

    public List<CourseDto> getAllCourses() {
        return coursesRepository.findAll().stream().map(courseMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public CourseDto getOneCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course courseForSearch = coursesRepository.getById(courseId);
        return courseMapper.toDto(courseForSearch);
    }

    @Transactional
    public CourseDto addNewCourse(CourseDto courseDto) {
        Course newCourse = Course.builder()
                .title(courseDto.getTitle())
                .description(courseDto.getDescription())
                .cost(courseDto.getCost())
                .build();
        coursesRepository.save(newCourse);

        return courseMapper.toDto(newCourse);
    }

    @Transactional
    public CourseDto updateCourse(Long courseId, CourseDto courseDto) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course courseForUpdate = coursesRepository.getById(courseId);

        courseForUpdate.setTitle(courseDto.getTitle());
        courseForUpdate.setDescription(courseDto.getDescription());
        courseForUpdate.setCost(courseDto.getCost());

        coursesRepository.save(courseForUpdate);
        return courseMapper.toDto(courseForUpdate);
    }

    public void deleteCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);

        coursesRepository.deleteById(courseId);
    }

    @Transactional
    public List<AccountDto> findAllAccountsWhoSignedUpForCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course course = coursesRepository.getById(courseId);

        return course.getAccounts().stream().map(accountMapper::toDto).collect(Collectors.toList());
    }


    @Transactional
    public List<LessonDto> addNewLessonOnCourse(Long courseId, LessonDto lessonDto) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course course = coursesRepository.getById(courseId);

        Lesson newLesson = Lesson.builder()
                .title(lessonDto.getTitle())
                .description(lessonDto.getDescription())
                .build();

        lessonsRepository.save(newLesson);
        course.getLessons().add(newLesson);

        return course.getLessons().stream().map(lessonMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public List<LessonDto> getAllLessonsOnCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course course = coursesRepository.getById(courseId);

        return course.getLessons().stream().map(lessonMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public List<AccountDto> getOnlyStudentsOnCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course course = coursesRepository.getById(courseId);

        List<Account> accounts = course.getAccounts();

        for (int i = 0; i < accounts.size(); i++) {
            Account account = accounts.get(i);
            if (account.getRole() == Role.ADMIN || account.getRole() == Role.MENTOR){
                accounts.remove(account);
            }
        }

        return accounts.stream().map(accountMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public Integer getCountOfLessonsOnCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);

        Course course = coursesRepository.getById(courseId);
        return course.getLessons().size();
    }

    @Transactional
    public Integer getCountOfStudentsOnCourse(Long courseId) {
        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);

        Course course = coursesRepository.getById(courseId);

        List<Account> accounts = course.getAccounts();

        for (int i = 0; i < accounts.size(); i++) {
            Account account = accounts.get(i);
            if (account.getRole() == Role.ADMIN || account.getRole() == Role.MENTOR){
                accounts.remove(account);
            }
        }
        
        return accounts.size();
    }
}
