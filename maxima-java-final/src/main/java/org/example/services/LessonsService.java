package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.LessonDto;
import org.example.exceptions.LessonNotFoundException;
import org.example.mappers.LessonMapper;
import org.example.models.Lesson;
import org.example.repositories.LessonsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LessonsService {
    private final LessonsRepository lessonsRepository;
    private final LessonMapper lessonMapper;
    @Transactional
    public List<LessonDto> getAllLessons() {
        return lessonsRepository.findAll().stream().map(lessonMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public LessonDto getOneLesson(Long lessonId) {
        lessonsRepository.findById(lessonId).orElseThrow(LessonNotFoundException::new);
        Lesson lesson = lessonsRepository.getById(lessonId);
        return lessonMapper.toDto(lesson);
    }

    @Transactional
    public LessonDto addNewLesson(LessonDto lessonDto) {
        Lesson lesson = Lesson.builder()
                .title(lessonDto.getTitle())
                .description(lessonDto.getDescription())
                .build();

        lessonsRepository.save(lesson);

        return lessonMapper.toDto(lesson);
    }

    @Transactional
    public LessonDto updateLesson(Long lessonId, LessonDto lessonDto) {
        lessonsRepository.findById(lessonId).orElseThrow(LessonNotFoundException::new);
        Lesson lessonForUpdate = lessonsRepository.getById(lessonId);
        lessonForUpdate.setTitle(lessonDto.getTitle());
        lessonForUpdate.setDescription(lessonDto.getDescription());

        return lessonMapper.toDto(lessonForUpdate);
    }

    @Transactional
    public void deleteLesson(Long lessonId) {
        lessonsRepository.findById(lessonId).orElseThrow(LessonNotFoundException::new);
        lessonsRepository.deleteById(lessonId);
    }
}
