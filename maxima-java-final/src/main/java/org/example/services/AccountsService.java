package org.example.services;

import lombok.RequiredArgsConstructor;
import org.example.dto.AccountDto;
import org.example.dto.CourseDto;
import org.example.dto.LessonDto;
import org.example.exceptions.AccountNotFoundException;
import org.example.exceptions.CourseNotFoundException;
import org.example.mappers.AccountMapper;
import org.example.mappers.CourseMapper;
import org.example.mappers.LessonMapper;
import org.example.models.Account;
import org.example.models.Course;
import org.example.models.Role;
import org.example.models.State;
import org.example.repositories.AccountsRepository;
import org.example.repositories.CoursesRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountsService {

    private final AccountsRepository accountsRepository;
    private final AccountMapper accountMapper;
    private final PasswordEncoder passwordEncoder;
    private final CourseMapper courseMapper;
    private final CoursesRepository coursesRepository;
    private final LessonMapper lessonMapper;

    public AccountDto getByEmail(String email) {
        return accountsRepository.findByEmail(email)
                .map(accountMapper::toDto)
                .orElseThrow(AccountNotFoundException::new);
    }

    public List<AccountDto> getAllAccounts() {
        return accountsRepository.findAll().stream().map(accountMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public AccountDto getOneAccount(Long accountId) {
        var account = accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        var accountForSearch = accountsRepository.getById(accountId);
        return accountMapper.toDto(accountForSearch);
    }

    public AccountDto addNewAccount(AccountDto accountDto) {
        Account newAccount = Account.builder()
                .firstName(accountDto.getFirstName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail())
                .password(passwordEncoder.encode(accountDto.getPassword()))
                .role(Role.USER)
                .state(State.NOT_CONFIRMED)
                .build();
        accountsRepository.save(newAccount);
        return accountMapper.toDto(newAccount);
    }

    @Transactional(readOnly = true)
    public AccountDto updateAccount(Long accountId, AccountDto accountDto) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        Account updatedAccount = accountsRepository.getById(accountId);

        updatedAccount.setFirstName(accountDto.getFirstName());
        updatedAccount.setLastName(accountDto.getLastName());
        updatedAccount.setEmail(accountDto.getEmail());
        updatedAccount.setPassword(passwordEncoder.encode(accountDto.getPassword()));

        accountsRepository.save(updatedAccount);
        return accountMapper.toDto(updatedAccount);
    }

    public void deleteAccount(Long accountId) {
        accountsRepository.deleteById(accountId);
    }

    @Transactional
    public void confirmedAccount(Long accountId) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        Account accountForConfirmed = accountsRepository.getById(accountId);

        accountForConfirmed.setState(State.CONFIRMED);
        accountsRepository.save(accountForConfirmed);
    }

    @Transactional
    public void setRoleAdminToAccount(Long accountId) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        Account accountForAdminRole = accountsRepository.getById(accountId);

        accountForAdminRole.setRole(Role.ADMIN);
        accountsRepository.save(accountForAdminRole);
    }

    @Transactional
    public void setRoleStudentToAccount(Long accountId) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        Account accountForStudentRole = accountsRepository.getById(accountId);

        accountForStudentRole.setRole(Role.STUDENT);
        accountsRepository.save(accountForStudentRole);
    }

    @Transactional
    public List<CourseDto> addCourseToAccount(Long accountId, Long courseId) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        Account account = accountsRepository.getById(accountId);

        coursesRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        Course course = coursesRepository.getById(courseId);

        account.getCourses().add(course);
        accountsRepository.save(account);

        return account.getCourses().stream().map(courseMapper::toDto).collect(Collectors.toList());
    }

    @Transactional
    public Integer getCountOfCoursesFromAccount(Long accountId) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);
        Account account = accountsRepository.getById(accountId);

        return account.getCourses().size();
    }

    @Transactional
    public List<LessonDto> getAllLessonsFromAccount(Long accountId) {
        accountsRepository.findById(accountId).orElseThrow(AccountNotFoundException::new);

        Account account = accountsRepository.getById(accountId);

        return account.getLessons().stream().map(lessonMapper::toDto).collect(Collectors.toList());
    }
}
