package org.example.models;

public enum State {
    CONFIRMED, NOT_CONFIRMED, BANNED
}
