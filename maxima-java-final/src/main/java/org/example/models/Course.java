package org.example.models;


import lombok.*;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;

@Transactional
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = {"accounts", "lessons"})
@EqualsAndHashCode(exclude = {"accounts", "lessons"})
@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private Integer cost;

    @ManyToMany(mappedBy = "courses")
    private List<Account> accounts;

    @OneToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "lesson_id", referencedColumnName = "id")})
    private List<Lesson> lessons;

}