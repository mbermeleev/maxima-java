package org.example.models;


import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "lessons")
@ToString(exclude = {"course", "account"})
@EqualsAndHashCode(exclude = {"course", "account"})
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;

    @ManyToOne
    private Course course;

    @ManyToOne
    private Account account;
}
