package org.example.exceptions;

public class LessonNotFoundException extends RuntimeException{

    public LessonNotFoundException(){
        super("This lesson not found!");
    }
}
