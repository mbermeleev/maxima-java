package org.example.exceptions;

public class CourseNotFoundException extends RuntimeException{

    public CourseNotFoundException() {
        super("This course not Found!");
    }
}
