package org.example.exceptions;

public class AccountNotFoundException extends RuntimeException{

    public AccountNotFoundException() {
        super("This account not found!");
    }
}
