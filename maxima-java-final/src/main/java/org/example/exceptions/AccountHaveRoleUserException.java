package org.example.exceptions;

public class AccountHaveRoleUserException extends RuntimeException{
    public AccountHaveRoleUserException(){
        super("This account only user!");
    }
}
