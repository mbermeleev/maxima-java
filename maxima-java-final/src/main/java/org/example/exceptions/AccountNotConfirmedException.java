package org.example.exceptions;

public class AccountNotConfirmedException extends RuntimeException{
    public AccountNotConfirmedException(){
        super("This account not confirmed!");
    }
}
