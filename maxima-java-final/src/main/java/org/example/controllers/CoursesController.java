package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.dto.*;
import org.example.repositories.CoursesRepository;
import org.example.services.CoursesService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/courses")
public class CoursesController {
    private final CoursesService coursesService;
    private final CoursesRepository coursesRepository;

    @GetMapping("/getAll")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<CourseResponse> getAllCourses() {
        return ResponseEntity.ok()
                .body(CourseResponse.builder().dtoList(coursesService.getAllCourses()).build());
    }

    @GetMapping("/getOne/{course_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<CourseDto> getOneCourse(@PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(coursesService.getOneCourse(courseId));
    }

    @PutMapping("/addCourse")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<CourseDto> addNewCourse(@RequestBody CourseDto courseDto) {
        return ResponseEntity.ok().body(coursesService.addNewCourse(courseDto));
    }

    @PatchMapping("/updateCourse/{course_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<CourseDto> updateCourse(@PathVariable("course_id" )Long courseId,
                                    @RequestBody CourseDto courseDto) {
        return ResponseEntity.ok().body(coursesService.updateCourse(courseId, courseDto));
    }

    @DeleteMapping("/deleteCourse/{course_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public void deleteCourse(@PathVariable("course_id") Long courseId) {
        coursesService.deleteCourse(courseId);
    }

    @GetMapping("/getAllAccounts/{course_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<AccountResponse> findAllAccountsWhoSignedUpForCourse(@PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(AccountResponse.builder()
                .dtoList(coursesService.findAllAccountsWhoSignedUpForCourse(courseId))
                .build());
    }

    @PatchMapping("/addLesson/{course_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<LessonResponse> addNewLessonOnCourse(@PathVariable("course_id") Long courseId,
                                                @RequestBody LessonDto lessonDto) {
        return ResponseEntity.ok().body(LessonResponse.builder()
                .dtoList(coursesService.addNewLessonOnCourse(courseId, lessonDto))
                .build());
    }

    @GetMapping("/getLessons/{course_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<LessonResponse> getAllLessonsOnCourse(@PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(LessonResponse.builder()
                .dtoList(coursesService.getAllLessonsOnCourse(courseId)).build());
    }

    @GetMapping("/getStudents/{course_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<AccountResponse> getOnlyStudentsOnCourse(@PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(AccountResponse.builder()
                .dtoList(coursesService.getOnlyStudentsOnCourse(courseId)).build());
    }

    @GetMapping("/getLessonsCount/{course_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<Integer> getCountOfLessonsOnCourse(@PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(coursesService.getCountOfLessonsOnCourse(courseId));
    }

    @GetMapping("/getStudentsCount/{course_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<Integer> getCountOfStudentsOnCourse(@PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(coursesService.getCountOfStudentsOnCourse(courseId));
    }
}
