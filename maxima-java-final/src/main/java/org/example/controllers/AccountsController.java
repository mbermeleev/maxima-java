package org.example.controllers;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.AccountDto;
import org.example.dto.AccountResponse;
import org.example.dto.CourseResponse;
import org.example.dto.LessonResponse;
import org.example.security.utils.SecurityUtils;
import org.example.services.AccountsService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/accounts")
@Slf4j
public class AccountsController {

    private final AccountsService accountsService;

    @GetMapping("/getAll")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<AccountResponse> getAllAccounts() {
        return ResponseEntity.ok()
                .body(AccountResponse.builder().dtoList(accountsService.getAllAccounts()).build());
    }

    @GetMapping("/getOne/{account_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<AccountDto> getOneAccount(@PathVariable("account_id") Long accountId) {
        return ResponseEntity.ok().body(accountsService.getOneAccount(accountId));
    }

    @PutMapping("/addAccount")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<AccountDto> addNewAccount(@RequestBody AccountDto accountDto) {
        return ResponseEntity.ok().body(accountsService.addNewAccount(accountDto));
    }

    @PatchMapping("/updateAccount/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<AccountDto> updateAccount(@PathVariable("account_id") Long accountId,
                                                    @RequestBody AccountDto accountDto) {
        return ResponseEntity.ok().body(accountsService.updateAccount(accountId, accountDto));
    }

    @DeleteMapping("/deleteAccount/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public void deleteAccount(@PathVariable("account_id") Long accountId) {
        log.debug("Admin with id {} wants to remove account with id {}", SecurityUtils.getId(), accountId);
        accountsService.deleteAccount(accountId);
    }

    @PatchMapping("/confirmedAccount/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public void confirmedAccount(@PathVariable("account_id") Long accountId) {
        accountsService.confirmedAccount(accountId);
    }

    @PatchMapping("/setRoleAdmin/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public void setRoleAdminToAccount(@PathVariable("account_id") Long accountId) {
        accountsService.setRoleAdminToAccount(accountId);
    }

    @PatchMapping("/setRoleStudent/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public void setRoleStudentToAccount(@PathVariable("account_id") Long accountId) {
        accountsService.setRoleStudentToAccount(accountId);
    }

    @PatchMapping("/addCourse/{account_id}/{course_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<CourseResponse> addCourseToAccount(@PathVariable("account_id") Long accountId,
                                                             @PathVariable("course_id") Long courseId) {
        return ResponseEntity.ok().body(CourseResponse.builder()
                .dtoList(accountsService.addCourseToAccount(accountId, courseId))
                .build());
    }

    @GetMapping("/getCountOfCourses/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<Integer> getCountOfCoursesFromAccount(@PathVariable("account_id") Long accountId) {
        return ResponseEntity.ok().body(accountsService.getCountOfCoursesFromAccount(accountId));
    }

    @GetMapping("/getAllLessons/{account_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<LessonResponse> getAllLessonsFromAccount(@PathVariable("account_id") Long accountId) {
        return ResponseEntity.ok().body(LessonResponse.builder()
                .dtoList(accountsService.getAllLessonsFromAccount(accountId)).build());
    }
}
