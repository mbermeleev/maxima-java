package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.dto.LessonDto;
import org.example.dto.LessonResponse;
import org.example.services.LessonsService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lessons")
public class LessonsController {

    private final LessonsService lessonsService;

    @GetMapping("/getAll")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<LessonResponse> getAllLessons() {
        return ResponseEntity.ok()
                .body(LessonResponse.builder().dtoList(lessonsService.getAllLessons()).build());
    }

    @GetMapping("/getOne/{lesson_id}")
    @PreAuthorize("hasAnyAuthority(" +
            "@authorities.ROLE_ADMIN," +
            "@authorities.ROLE_MENTOR," +
            "@authorities.ROLE_STUDENT)")
    public ResponseEntity<LessonDto> getOneLesson(@PathVariable("lesson_id") Long lessonId) {
        return ResponseEntity.ok().body(lessonsService.getOneLesson(lessonId));
    }

    @PutMapping("/addLesson")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<LessonDto> addNewLesson(@RequestBody LessonDto lessonDto) {
        return ResponseEntity.ok().body(lessonsService.addNewLesson(lessonDto));
    }

    @PatchMapping("/updateAccount/{lesson_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public ResponseEntity<LessonDto> updateLesson(@PathVariable("lesson_id" )Long lessonId,
                                    @RequestBody LessonDto lessonDto) {
        return ResponseEntity.ok().body(lessonsService.updateLesson(lessonId, lessonDto));
    }

    @DeleteMapping("/deleteLesson/{lesson_id}")
    @PreAuthorize("hasAuthority(@authorities.ROLE_ADMIN)")
    public void deleteLesson(@PathVariable("lesson_id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
    }
}
