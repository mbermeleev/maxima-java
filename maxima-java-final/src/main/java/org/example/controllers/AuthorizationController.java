package org.example.controllers;

import lombok.RequiredArgsConstructor;
import org.example.dto.AuthDTO;
import org.example.services.AuthService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthorizationController {
    private final AuthService authService;

    @PostMapping("/auth")
    public String token(@RequestBody AuthDTO authDTO) {
        return authService.authorize(authDTO);
    }
}
