package org.example.mappers;

import org.example.dto.LessonDto;
import org.example.models.Lesson;
import org.mapstruct.Mapper;

@Mapper
public interface LessonMapper {
    LessonDto toDto(Lesson entity);
    Lesson toEntity(LessonDto dto);
}
