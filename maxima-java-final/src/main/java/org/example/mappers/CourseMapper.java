package org.example.mappers;

import org.example.dto.CourseDto;
import org.example.models.Course;
import org.mapstruct.Mapper;

@Mapper
public interface CourseMapper {
    CourseDto toDto(Course entity);
    Course toEntity(CourseDto dto);
}
