package org.example.mappers;

import org.example.dto.AccountDto;
import org.example.models.Account;
import org.mapstruct.Mapper;

@Mapper
public interface AccountMapper {
    AccountDto toDto(Account entity);
    Account toEntity(AccountDto dto);
}
