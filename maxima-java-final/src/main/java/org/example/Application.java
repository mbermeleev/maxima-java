package org.example;

import lombok.RequiredArgsConstructor;
import org.example.models.*;
import org.example.properties.RsaKeyProperties;
import org.example.repositories.AccountsRepository;
import org.example.repositories.CoursesRepository;
import org.example.repositories.LessonsRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication()
@EnableConfigurationProperties(RsaKeyProperties.class)
@RequiredArgsConstructor
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private final CoursesRepository coursesRepository;
    private final AccountsRepository accountsRepository;
    private final LessonsRepository lessonsRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        Lesson lesson1Java = Lesson.builder()
                .title("Lesson1")
                .description("Lesson1")
                .build();
        Lesson lesson2Java = Lesson.builder()
                .title("Lesson2")
                .description("Lesson2")
                .build();
        Lesson lesson3Java = Lesson.builder()
                .title("Lesson3")
                .description("Lesson3")
                .build();

        Lesson lesson1Sql = Lesson.builder()
                .title("Lesson1")
                .description("Lesson1")
                .build();
        Lesson lesson2Sql = Lesson.builder()
                .title("Lesson2")
                .description("Lesson2")
                .build();
        Lesson lesson3Sql = Lesson.builder()
                .title("Lesson3")
                .description("Lesson3")
                .build();

        Lesson lesson1Spring = Lesson.builder()
                .title("Lesson1")
                .description("Lesson1")
                .build();
        Lesson lesson2Spring = Lesson.builder()
                .title("Lesson2")
                .description("Lesson2")
                .build();
        Lesson lesson3Spring = Lesson.builder()
                .title("Lesson3")
                .description("Lesson3")
                .build();

        lessonsRepository.save(lesson1Java);
        lessonsRepository.save(lesson2Java);
        lessonsRepository.save(lesson3Java);
        lessonsRepository.save(lesson1Sql);
        lessonsRepository.save(lesson2Sql);
        lessonsRepository.save(lesson3Sql);
        lessonsRepository.save(lesson1Spring);
        lessonsRepository.save(lesson2Spring);
        lessonsRepository.save(lesson3Spring);


        Course java = Course.builder()
                .title("Java")
                .description("Java")
                .cost(30000)
                .build();
        Course sql = Course.builder()
                .title("SQL")
                .description("SQL")
                .cost(30000)
                .build();
        Course spring = Course.builder()
                .title("Spring framework")
                .description("Spring framework")
                .cost(30000)
                .build();

        List<Lesson> javaLessons = new ArrayList<>();
        javaLessons.add(lesson1Java);
        javaLessons.add(lesson2Java);
        javaLessons.add(lesson3Java);
        java.setLessons(javaLessons);

        List<Lesson> sqlLessons = new ArrayList<>();
        sqlLessons.add(lesson1Sql);
        sqlLessons.add(lesson2Sql);
        sqlLessons.add(lesson3Sql);
        sql.setLessons(sqlLessons);

        List<Lesson> springLessons = new ArrayList<>();
        sqlLessons.add(lesson1Spring);
        sqlLessons.add(lesson2Spring);
        sqlLessons.add(lesson3Spring);
        spring.setLessons(springLessons);

        coursesRepository.save(java);
        coursesRepository.save(sql);
        coursesRepository.save(spring);

        Account account1 = Account.builder()
                .firstName("Admin")
                .lastName("Admin")
                .email("Admin@mail.ru")
                .password(passwordEncoder.encode("Admin"))
                .role(Role.ADMIN)
                .state(State.CONFIRMED)
                .build();
        Account account2 = Account.builder()
                .firstName("Mentor")
                .lastName("Mentor")
                .email("Mentor@mail.ru")
                .password(passwordEncoder.encode("Mentor"))
                .role(Role.MENTOR)
                .state(State.CONFIRMED)
                .build();
        Account account3 = Account.builder()
                .firstName("Student1")
                .lastName("Student1")
                .email("Student1@mail.ru")
                .password(passwordEncoder.encode("Student1"))
                .role(Role.STUDENT)
                .state(State.CONFIRMED)
                .build();
        Account account4 = Account.builder()
                .firstName("Student2")
                .lastName("Student2")
                .email("Student2@mail.ru")
                .password(passwordEncoder.encode("Student2"))
                .role(Role.STUDENT)
                .state(State.CONFIRMED)
                .build();
        Account account5 = Account.builder()
                .firstName("Student3")
                .lastName("Student3")
                .email("Student3@mail.ru")
                .password(passwordEncoder.encode("Student3"))
                .role(Role.STUDENT)
                .state(State.CONFIRMED)
                .build();

        List<Course> account2Courses = new ArrayList<>();
        account2Courses.add(java);
        account2Courses.add(sql);
        account2Courses.add(spring);
        account2.setCourses(account2Courses);

        List<Course> account3Courses = new ArrayList<>();
        account3Courses.add(java);
        account3Courses.add(sql);
        account3Courses.add(spring);
        account3.setCourses(account3Courses);

        List<Course> account4Courses = new ArrayList<>();
        account4Courses.add(java);
        account4Courses.add(sql);
        account4.setCourses(account4Courses);

        List<Course> account5Courses = new ArrayList<>();
        account5Courses.add(java);
        account5.setCourses(account5Courses);

        accountsRepository.save(account1);
        accountsRepository.save(account2);
        accountsRepository.save(account3);
        accountsRepository.save(account4);
        accountsRepository.save(account5);
    }

}
