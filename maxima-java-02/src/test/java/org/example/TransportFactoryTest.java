package org.example;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
public class TransportFactoryTest extends TransportFactory{

    /* Тесты приватных методов не должны тестироваться
    @Test
    public void shouldTransportsSpeedBe300(){
        City kazan = new City("Kazan", 1500, true, true);
        int speed = (int) TransportFactory.speedRounding(kazan, 5);
        assertEquals(300, speed);
    }
    @Test
    public void shouldTransportsCapacityBe4000(){
        int capacity = (int) TransportFactory.capacityRounding(3700);
        assertEquals(4000, capacity);
    }*/
    private City kazan;
    private City city;
    @Before
    public void prepare(){
        city = new City("city", 1500, false, false);
        kazan = new City("Kazan", 1500, true, true);
    }
    @Test
    public void shouldTransportBePlane(){
        Plane plane = new Plane("Plane", 4000.0, (int) 300.0, 200);
        assertTrue(getTransport(kazan, 3700, 5).equals(plane));
    }
    @Test
    public void shouldTransportBeShip(){
        Ship ship = new Ship("Ship", 4000.0,40, 115);
        assertTrue(getTransport(kazan, 3700, 38).equals(ship));
    }
    @Test
    public void shouldTransportBeTruck(){
        Truck truck = new Truck("Truck", 4000.0, 60, 100);
        assertTrue(getTransport(kazan, 3700, 26).equals(truck));
    }
    @Test
    public void shouldnotTransportBePlane(){
        Truck truck = new Truck("Truck", 4000.0, 150, 100);
        assertTrue(getTransport(city, 3700, 10).equals(truck));
    }
    @Test
    public void shouldnotTransportBeShip(){
        Truck truck = new Truck("Truck", 4000.0, 40, 100);
        assertTrue(getTransport(city, 3700, 38).equals(truck));
    }
    @Test
    public void shouldSpeedBe380(){
        Plane plane = new Plane("Plane", 4000, 380, 125);
        assertEquals(getTransport(kazan, 3700, 4).getSpeed(), plane.getSpeed());
    }
    @Test
    public void shouldCapacityBe4000(){
        Plane plane = new Plane("Plane", 4000, 380, 125);
        assertEquals((int)getTransport(kazan, 3700, 4).getCapacity(),(int) plane.getCapacity());
    }
}