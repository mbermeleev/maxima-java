package org.example;

public class Logistics {

    private Transport[] vehicles;

    public Logistics(Transport ... vehicles) {
        this.vehicles = vehicles;
    }

    public void setVehicles(Transport[] vehicles) {
        this.vehicles = vehicles;
    }

    public Transport[] getVehicles() {
        return vehicles;
    }

    public Transport getShipping(City city, int weight, int hours) {

        double minPrice = 0;
        Transport vehicle = null;
        int i = 0;
        while (i < vehicles.length){
            if (isShippingAvailable(city, vehicles[i], weight, hours)) {
                if (minPrice == 0 || minPrice > vehicles[i].getPrice(city)){
                    minPrice = vehicles[i].getPrice(city);
                    vehicle = vehicles[i];
                }
            }
            i++;
        }
        return vehicle;
    }
    private boolean isShippingAvailable(City city, Transport transport, int weight, int hours){

        int deliveryTime = city.getDistanceKm() / transport.getSpeed();

        if (deliveryTime <= hours && transport.getCapacity() >= weight && !transport.isRepairing()
                && transport.getPrice(city) != 0){
            return true;
        }
            return false;
    }
}