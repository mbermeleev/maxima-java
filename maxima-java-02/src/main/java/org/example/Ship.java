package org.example;

public class Ship extends Transport implements Repairable{
    public Ship(String name, double capacity, int speed, double costOfKm) {
        super(name, capacity, speed, costOfKm);
    }
    @Override
    public float getPrice(City city) {

        return city.isOnWater() ? (float) (city.getDistanceKm() * getCostOfKm()) : 0;
    }
}
