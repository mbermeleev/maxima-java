package org.example;
public class TransportFactory {
    Transport getTransport(City city, int weight, int hours){

        double capacity = capacityRounding(weight);

        double speed = speedRounding(city, hours);

        if (city.getDistanceKm() / hours <= 40 && city.isOnWater()){
            return new Ship("Ship", capacity, (int) speed, 115);
        }
        if (city.getDistanceKm() / hours > 120 && city.hasAirport()) {
            return new Plane("Plane", capacity, (int) speed, 200);
        }
        return new Truck("Truck", capacity, (int) speed, 100);
    }
    private static double speedRounding(City city, int hours) {
        double speed = city.getDistanceKm() / hours;
        return speed % 10 != 0 ? (Math.ceil(speed / 10) * 10) : speed;
    }
    private static double capacityRounding(int weight) {
        return (double) weight % 500 != 0 ? (Math.ceil( (double) weight / 500) * 500) : weight;
    }
}
