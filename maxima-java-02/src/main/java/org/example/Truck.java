package org.example;

public class Truck extends Transport implements Repairable{
    public Truck(String name, double capacity, int speed, double costOfKm) {
        super(name, capacity, speed, costOfKm);
    }
    @Override
    public float getPrice(City city) {

        return city.hasRailway() ? (float) (city.getDistanceKm() * getCostOfKm()) : 0;
    }
}