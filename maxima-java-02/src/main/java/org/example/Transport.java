package org.example;

import java.util.Objects;

abstract class Transport implements Repairable {
    private String name;
    private double capacity;
    private int speed;
    private double costOfKm;
    private boolean broken;

    public Transport(String name, double capacity, int speed, double costOfKm) {
        this.name = name;
        this.capacity = capacity;
        this.speed = speed;
        this.costOfKm = costOfKm;
    }

    public String getName() {
        return name;
    }

    public double getCapacity() {
        return capacity;
    }

    public int getSpeed() {
        return speed;
    }

    public double getCostOfKm() {
        return costOfKm;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setCostOfKm(double costOfKm) {
        this.costOfKm = costOfKm;
    }

    public boolean isBroken() {
        return broken;
    }

    public void setBroken(boolean broken) {
        this.broken = broken;
    }

    abstract float getPrice(City city);

    @Override
    public void startRepair() {
        broken = true;
    }

    @Override
    public void finishRepair() {
        broken = false;
    }

    @Override
    public boolean isRepairing() {
        return broken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transport transport = (Transport) o;
        return Double.compare(transport.capacity, capacity) == 0 && speed == transport.speed && Double.compare(transport.costOfKm, costOfKm) == 0 && broken == transport.broken && Objects.equals(name, transport.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, capacity, speed, costOfKm, broken);
    }

    @Override
    public String toString() {
        return "Transport{" +
                "name='" + name + '\'' +
                ", capacity=" + capacity +
                ", speed=" + speed +
                ", costOfKm=" + costOfKm +
                ", broken=" + broken +
                '}';
    }
}
