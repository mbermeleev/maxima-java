package org.example;

public class App {

    public static void main( String[] args ) {
        City kazan = new City("Kazan", 1500, true, true);
        Transport plane = new Plane("Plane", 600, 500, 136.3);
        Transport ship = new Ship("Ship", 500, 60, 115.3);
        Transport truck = new Truck("Truck", 700, 40, 500);
        Transport ship2 = new Ship("Ship", 500, 60, 110.3);
        Transport plane2 = new Plane("Plane", 600, 500, 116.3);
        Logistics logistics = new Logistics(plane, ship, truck, ship2, plane2);
        ship2.startRepair();
        ship.startRepair();
        ship.finishRepair();

        //System.out.println(logistics.getShipping(kazan, 300,5));

   }
}

