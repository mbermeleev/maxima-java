package org.example;

public class Plane extends Transport implements Repairable{
    public Plane(String name, double capacity, int speed, double costOfKm) {
        super(name, capacity, speed, costOfKm);
    }
    @Override
    public float getPrice(City city) {

        return city.hasAirport() ? (float) (city.getDistanceKm() * getCostOfKm()) : 0;
    }

}
