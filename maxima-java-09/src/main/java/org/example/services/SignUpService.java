package org.example.services;

import org.example.dto.SignUpForm;
import org.example.models.Account;
import org.example.repositories.AccountsJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {

    private final AccountsJpaRepository accountsJpaRepository;
    @Autowired
    public SignUpService(AccountsJpaRepository accountsJpaRepository) {
        this.accountsJpaRepository = accountsJpaRepository;
    }

    public void signUp(SignUpForm signUpForm){
        Account account = Account.builder()
                .firstName(signUpForm.getFirstName())
                .lastName(signUpForm.getLastName())
                .email(signUpForm.getEmail())
                .password(signUpForm.getPassword())
                .age(signUpForm.getAge())
                .build();

        accountsJpaRepository.save(account);
    }
}
