package org.example.services;

import org.example.dto.AccountDto;
import org.example.repositories.AccountsJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.example.dto.AccountDto.from;

@Service
public class AccountsService {

    private final AccountsJpaRepository accountsJpaRepository;

    @Autowired
    public AccountsService(AccountsJpaRepository accountsJpaRepository) {
        this.accountsJpaRepository = accountsJpaRepository;
    }

    public List<AccountDto> getAllAccounts() {
        return from(accountsJpaRepository.findAll());
    }
}
