package org.example.repositories;

import org.example.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public interface AccountsJpaRepository extends JpaRepository<Account, Long> {
}
