package org.example.controllers;

import org.example.services.AccountsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/accounts")
public class AccountsController {

    private final AccountsService accountsService;

    public AccountsController(AccountsService accountsService) {
        this.accountsService = accountsService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getAllAccounts(Model model) {
        model.addAttribute("accounts", accountsService.getAllAccounts());
        return "accounts";
    }
}
