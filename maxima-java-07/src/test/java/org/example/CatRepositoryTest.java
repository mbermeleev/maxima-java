package org.example;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.example.model.Cat;
import org.example.repository.CatRepository;
import org.example.repository.AdvancedCatRepository;
import org.junit.Before;
import org.junit.Test;

import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.*;

public class CatRepositoryTest {
    //private static final String DB_URL = "jdbc:h2:mem:test";
    private CatRepository repository;

    @Before
    public void init() {
        repository = new AdvancedCatRepository();
    }

    @Test
    public void shouldCRUDOperationWorks(){
        Cat cat1 = new Cat(true, "cat1", 1, 1L);
        Cat cat2 = new Cat(false, "cat2", 2, 2L);
        Cat cat3 = new Cat(true, "cat3", 3, 3L);
        Cat cat4 = new Cat(false, "cat4", 4, 4L);
        Cat cat5 = new Cat(true, "cat5", 5, 5L);

        repository.create(cat1);
        repository.create(cat2);
        repository.create(cat3);
        repository.create(cat4);
        repository.create(cat5);

        List<Cat> cats = repository.findAll();
        assertEquals(5, cats.size());

        Cat testCat = repository.read(3L);
        assertEquals("cat3", testCat.getName());
        assertEquals(3, testCat.getWeight());
        assertTrue( testCat.isAngry());

        Cat newCat1 = new Cat(false, "murzik", 1, 1L);
        Cat newCat4 = new Cat(true, "cat4", 7, 4L);

        repository.update(newCat1.getId(), newCat1);
        repository.update(newCat4.getId(), newCat4);

        testCat = repository.read(4L);
        assertEquals("cat4", testCat.getName());
        assertTrue(testCat.isAngry());
        assertEquals(7, testCat.getWeight());

        repository.delete(2L);

        testCat = repository.read(2L);
        assertNull(testCat);

        repository.delete(4L);

        cats = repository.findAll();
        assertEquals(3, cats.size());
    }
}
