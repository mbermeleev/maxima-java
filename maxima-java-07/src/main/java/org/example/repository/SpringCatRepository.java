package org.example.repository;

import org.example.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
@Repository
public class SpringCatRepository implements CatRepository{

    //language=SQL
    private static final String SQL_CREATE_TABLE = "CREATE TABLE cats_table (isangry VARCHAR(10), name VARCHAR(20), weight INT, id INT)";
    // language=SQL
    private static final String SQL_INSERT = "insert into cats_table(isangry, name, weight, id) values (?, ?, ?, ?)";
    // language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from cats_table where id = ?";
    //language=SQL
    private static final String SQL_UPDATE = "update cats_table set isangry = ?, name = ?, weight = ? where id = ?";
    //language=SQL
    private static final String SQL_DELETE = "delete from cats_table where id = ?";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from cats_table";

    private static final RowMapper<Cat> catMapper = (row, rowNamber) -> new Cat(
            row.getBoolean("isangry"),
            row.getString("name"),
            row.getInt("weight"),
            row.getLong("id"));

    private final JdbcTemplate template;

    @Autowired
    public SpringCatRepository(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }
    public void createTable(){
        template.execute(SQL_CREATE_TABLE);
    }
    @Override
    public boolean create(Cat element) {
        return template.update(SQL_INSERT,
                element.isAngry(),
                element.getName(),
                element.getWeight(),
                element.getId()) > 0;
    }
    @Override
    public Cat read(Long id) {
        return template.queryForObject(SQL_SELECT_BY_ID, catMapper, id);
    }
    @Override
    public int update(Long id, Cat element) {
        return template.update(SQL_UPDATE,
                element.isAngry(),
                element.getName(),
                element.getWeight(), id);
    }
    @Override
    public void delete(Long id) {
        template.update(SQL_DELETE, id);
    }
    @Override
    public List<Cat> findAll() {
        return new ArrayList<>(template.query(SQL_SELECT_ALL, catMapper));
    }
}
