package org.example.repository;

import org.example.model.Cat;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
public class SimpleCatRepository implements CatRepository {
    private final DataSource dataSource;
    // language=SQL
    private static final String SQL_INSERT = "insert into cats_table(isangry, name, weight, id) values (?, ?, ?, ?)";
    // language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from cats_table where id = ?";
    //language=SQL
    private static final String SQL_UPDATE = "update cats_table set isangry = ?, name = ?, weight = ? where id = ?";
    //language=SQL
    private static final String SQL_DELETE = "delete from cats_table where id = ?";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select isangry, name, weight, id from cats_table order by id";
    private static final Function<ResultSet, Cat> catMapper = resultSet -> {
        try {
            Long resultId = resultSet.getLong("id");
            String name = resultSet.getString("name");
            int weight = resultSet.getInt("weight");
            boolean isAngry = resultSet.getBoolean("isangry");
            return new Cat(isAngry, name, weight, resultId);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };
    public SimpleCatRepository(DataSource dataSource) {
        this.dataSource = dataSource;
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE cats_table (isangry VARCHAR(10), name VARCHAR(20), weight INT, id INT)");
        } catch (SQLException e) {
            throw new IllegalArgumentException("Table wasn't made!");
        }
    }
    @Override
    public boolean create(Cat element) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT)){
            preparedStatement.setBoolean(1, element.isAngry());
            preparedStatement.setString(2, element.getName());
            preparedStatement.setInt(3, element.getWeight());
            preparedStatement.setLong(4, element.getId());

            int rows = preparedStatement.executeUpdate();
            if (rows != 1) {
                throw new SQLException("Didn't select!");
            }
            return true;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public Cat read(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return catMapper.apply(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public int update(Long id, Cat element) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setBoolean(1, element.isAngry());
            statement.setString(2, element.getName());
            statement.setInt(3, element.getWeight());
            statement.setLong(4, id);

            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void delete(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE)) {
            statement.setLong(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException("This cat didn't delete!");
        }
    }
    @Override
    public List<Cat> findAll() {
        List<Cat> cats = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                cats.add(catMapper.apply(resultSet));
            }
            return cats;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
