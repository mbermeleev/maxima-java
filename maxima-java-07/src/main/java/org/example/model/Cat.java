package org.example.model;

import java.util.StringJoiner;

public class Cat {
    private Long id;
    private boolean isAngry;
    private String name;
    private int weight;
    public Cat(boolean isAngry, String name, int weight, Long id) {
        this.id = id;
        this.isAngry = isAngry;
        this.name = name;
        this.weight = weight;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Cat.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("isAngry=" + isAngry)
                .add("name='" + name + "'")
                .add("weight=" + weight)
                .toString();
    }
}
