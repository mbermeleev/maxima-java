package org.example.app;

import org.example.config.AnnotationConfig;
import org.example.model.Cat;
import org.example.repository.SpringCatRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        int i = 0;
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AnnotationConfig.class);
        SpringCatRepository repository = applicationContext.getBean(SpringCatRepository.class);
        repository.createTable();

        Cat cat1 = new Cat(true, "cat1", 1, 1L);
        Cat cat2 = new Cat(false, "cat2", 2, 2L);
        Cat cat3 = new Cat(true, "cat3", 3, 3L);
        Cat cat4 = new Cat(false, "cat4", 4, 4L);
        Cat cat5 = new Cat(true, "cat5", 5, 5L);

        repository.create(cat1);
        repository.create(cat2);
        repository.create(cat3);
        repository.create(cat4);
        repository.create(cat5);
        repository.findAll().forEach(System.out::println);

        System.out.println();
        System.out.println(repository.read(1L));

        Cat newCat4 = new Cat(true, "cat4", 7, 4L);
        repository.update(newCat4.getId(), newCat4);

        System.out.println();
        repository.findAll().forEach(System.out::println);

        repository.delete(2L);

        System.out.println();
        repository.findAll().forEach(System.out::println);
    }
}
