package org.example.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.example.model.Cat;
import org.example.repository.AdvancedCatRepository;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class App {
    public static void main( String[] args ) {

            AdvancedCatRepository repository = new AdvancedCatRepository();

            Cat cat1 = new Cat(true, "cat1", 1, 1L);
            Cat cat2 = new Cat(false, "cat2", 2, 2L);
            Cat cat3 = new Cat(true, "cat3", 3, 3L);
            Cat cat4 = new Cat(false, "cat4", 4, 4L);
            Cat cat5 = new Cat(true, "cat5", 5, 5L);

            repository.create(cat1);
            repository.create(cat2);
            repository.create(cat3);
            repository.create(cat4);
            repository.create(cat5);
            repository.findAll();
            repository.read(3L);

            Cat newCat4 = new Cat(true, "cat4", 7, 4L);
            repository.update(newCat4.getId(), newCat4);

            repository.findAll();

            repository.delete(1L);

            repository.findAll();

    }
}
